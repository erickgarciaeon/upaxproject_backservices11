package com.inicio.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inicio.dao.EmpleadoDAO;
import com.inicio.model.Employee;
import com.inicio.service.EmpleadoService;

@Service
public class EmpleadoServiceImpl implements EmpleadoService{
	
	@Autowired
	EmpleadoDAO dao;

	@Override
	public Employee agregarEmpleado(Employee empleado) {
		//VALIDA SI EL EMPLEADO EXISTE
		Employee resultado = dao.validaEmpleado(empleado.getName(), empleado.getLast_name());
		validaFecha(empleado.getBirthdate());
		if(resultado == null) {
			return dao.agregarEmpleado(empleado);
		}
		return null;
	}

	@Override
	public List<Employee> recuperarEmpleados() {
		return dao.recuperarEmpleados();
	}
	
	boolean validaFecha(Date fecha) {
		Date fechaActual = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("yyyy/MM/dd");
        String fechaSistema=formateador.format(fechaActual);
        String fechaIngresada = formateador.format(fecha);
		return false;
		
	}

	
}
