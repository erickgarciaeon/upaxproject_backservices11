package com.inicio.service;

import java.util.List;

import com.inicio.model.Employee;


public interface EmpleadoService {

	Employee agregarEmpleado(Employee empleado);
	List<Employee> recuperarEmpleados();
}
