package com.inicio.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the employees database table.
 * 
 */
@Entity
@Table(name="employees")
@NamedQuery(name="Employee.findAll", query="SELECT e FROM Employee e")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Temporal(TemporalType.DATE)
	private Date birthdate;

	@Column(name="LAST_NAME")
	private String last_name;

	private String name;
	@Column(name="JOB_ID")
	private long job_id;
	@Column(name="GENDER_ID")
	private long gender_id;

	
	

	public Employee() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getBirthdate() {
		return this.birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	
	}

	public String getLast_name() {
		return last_name;
	}

	public long getJob_id() {
		return job_id;
	}

	public long getGender_id() {
		return gender_id;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public void setJob_id(long job_id) {
		this.job_id = job_id;
	}

	public void setGender_id(long gender_id) {
		this.gender_id = gender_id;
	}
	
	
	}