package com.inicio.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the employee_worked_hours database table.
 * 
 */
@Entity
@Table(name="employee_worked_hours")
@NamedQuery(name="EmployeeWorkedHour.findAll", query="SELECT e FROM EmployeeWorkedHour e")
public class EmployeeWorkedHour implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	@Temporal(TemporalType.DATE)
	@Column(name="WORKED_DATE")
	private Date workedDate;

	@Column(name="WORKED_HOURD")
	private BigDecimal workedHourd;

	//bi-directional many-to-one association to Employee
	@ManyToOne
	private Employee employee;

	public EmployeeWorkedHour() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getWorkedDate() {
		return this.workedDate;
	}

	public void setWorkedDate(Date workedDate) {
		this.workedDate = workedDate;
	}

	public BigDecimal getWorkedHourd() {
		return this.workedHourd;
	}

	public void setWorkedHourd(BigDecimal workedHourd) {
		this.workedHourd = workedHourd;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}