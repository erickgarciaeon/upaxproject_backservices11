package com.inicio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.inicio.model.Employee;
import com.inicio.model.Response1;
import com.inicio.service.EmpleadoService;



@CrossOrigin(origins = "*")
@RestController
public class EmpleadoController {
	
	@Autowired
	EmpleadoService service;
	
	@GetMapping(value = "contrato", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Employee> recuperarContactos(){
		return service.recuperarEmpleados();		
	}
	
	@PostMapping(value = "contrato1", consumes = MediaType.APPLICATION_JSON_VALUE, produces =  MediaType.APPLICATION_JSON_VALUE)
	public Response1 guardarContactos(@RequestBody Employee empleado){
		Employee respuesta = new Employee();
		Response1 response1 = new Response1();
		respuesta = service.agregarEmpleado(empleado);
		if(respuesta != null) {
			response1.setId(String.valueOf(respuesta.getId()));
			response1.setRespuesta(true);
		}else {
			response1.setId(null);
			response1.setRespuesta(false);
		}
		
		return response1;		
	}

}
