package com.inicio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = { "com.inicio.controller", "com.inicio.service", "com.inicio.dao" })
@EntityScan(basePackages = { "com.inicio.model" })
@EnableJpaRepositories(basePackages = { "com.inicio.dao" })
@SpringBootApplication
public class ExamenUpaxApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenUpaxApplication.class, args);
	}

}
