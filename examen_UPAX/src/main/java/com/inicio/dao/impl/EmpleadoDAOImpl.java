package com.inicio.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.inicio.dao.EmpleadoDAO;
import com.inicio.dao.EmpleadoJpa;
import com.inicio.model.Employee;

@Repository
public class EmpleadoDAOImpl implements EmpleadoDAO{
	
	@Autowired
	EmpleadoJpa jpaSpring;


	@Override
	public Employee agregarEmpleado(Employee empleado) {
		return jpaSpring.save(empleado);
	}


	@Override
	public List<Employee> recuperarEmpleados() {
		return jpaSpring.findAll();
	}


	@Override
	public void eliminaEmpleado(String nombre) {
//		jpaSpring.eliminarPorNombre(nombre);
	}


	@Override
	public Employee validaEmpleado(String nombre, String apellido) {
		Employee resultado = new Employee();
		resultado = jpaSpring.validaEmpleado(nombre, apellido);
		return resultado;
	}

}
