package com.inicio.dao;

import java.util.List;

import com.inicio.model.Employee;

public interface EmpleadoDAO {
	
	Employee agregarEmpleado(Employee empleado);
	List<Employee> recuperarEmpleados();
	void eliminaEmpleado(String email);
	Employee validaEmpleado(String nombre, String apellido);
	


}
