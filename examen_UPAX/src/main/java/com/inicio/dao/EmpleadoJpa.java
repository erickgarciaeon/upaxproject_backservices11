package com.inicio.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.inicio.model.Employee;


public interface EmpleadoJpa extends JpaRepository<Employee, Integer>{
	
	Employee findByName(String nombre);
	
	@Transactional
	@Modifying
	@Query("Delete from Employee c where c.name=?1")
	void eliminarPorNombre(String nombre);
	
	@Query("select c from Employee c where c.name=?1 AND c.last_name=?2")
	Employee validaEmpleado(String nombre, String apellido);

}
